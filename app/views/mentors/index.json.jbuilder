json.array!(@mentors) do |mentor|
  json.extract! mentor, :id, :name, :sex, :age, :DOB, :bio, :image
  json.url mentor_url(mentor, format: :json)
end
