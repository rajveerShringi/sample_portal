json.array!(@students) do |student|
  json.extract! student, :id, :mentor_id, :name, :sex, :age, :bio, :DOB, :current_school, :current_level, :country, :sat_score
  json.url student_url(student, format: :json)
end
