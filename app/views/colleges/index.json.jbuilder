json.array!(@colleges) do |college|
  json.extract! college, :id, :name, :country, :sat_max_score, :sat_min_score, :tuition_fees
  json.url college_url(college, format: :json)
end
